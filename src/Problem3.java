import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number01;
        int number02;
        System.out.print("Please input first number: ");
        number01 = sc.nextInt();

        System.out.print("Please input second number: ");
        number02 = sc.nextInt();

        if (number01<=number02) {
            while (number01<=number02){
                System.out.print(number01+ " ");
                number01++;
            }
        } else {
            System.out.println("Error");
        }
    }
}
